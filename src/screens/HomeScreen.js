import React from 'react';
import { Text, StyleSheet, View, Button } from 'react-native';

const HomeScreen = ({navigation}) => {
  return (
    <View>
      <Text style={styles.text}>Hi there!</Text>
      <Button 
        style={styles.btnStyle}
        title="Go to Components Demo"
        onPress={() => navigation.navigate('Components')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to List Demo"
        onPress={() => navigation.navigate('List')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Image Screen"
        onPress={() => navigation.navigate('Image')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Counter Screen"
        onPress={() => navigation.navigate('Counter')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Colors Screen"
        onPress={() => navigation.navigate('Color')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Square Screen"
        onPress={() => navigation.navigate('Square')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Text Screen"
        onPress={() => navigation.navigate('Text')} 
      />
      <Button 
        style={styles.btnStyle}
        title="Go to Box Screen"
        onPress={() => navigation.navigate('Box')} 
      />
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  },
  btnStyle: {

  }
});

export default HomeScreen;
