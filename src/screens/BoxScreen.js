import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const BoxScreen = () => {
    return (
        <View style={styles.viewStyle}>
            <Text style={styles.textOneStyle}>Child #1</Text>
            <Text style={styles.textTwoStyle}>Child #2</Text>
            <Text style={styles.textThreeStyle}>Child #3</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    viewStyle: {
        borderWidth: 3,
        borderColor: 'black',
        // alignItems: 'flex-start',
        //alignItems: 'center',
        //alignItems: 'flex-end'
        //flexDirection: 'row',
        height: 200,
        //flexDirection: 'row',
        //justifyContent: 'space-around'
    },
    textOneStyle: {
        borderWidth: 3,
        borderColor: 'red',
        top: 10,
        //bottom: 10,
        //left: 10,
        right: 10
    },
    textTwoStyle: {
        borderWidth: 3,
        borderColor: 'red',
        //alignSelf: 'stretch'
        fontSize: 18,
        ...StyleSheet.absoluteFillObject

    },
    textThreeStyle: {
        borderWidth: 3,
        borderColor: 'red',

    }
});

export default BoxScreen;